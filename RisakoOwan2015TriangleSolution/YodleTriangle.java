import java.io.*;
import java.util.*;
import java.lang.*;

/*
** @author Risako Owan
** Written on March 21, 2015
** Problem from http://www.yodlecareers.com/puzzles/triangle.html
*/
public class YodleTriangle{
	/*
	** Takes an array of strings and returns it as an array of doubles
	** Called in method: readFile
	*/
	public static Double[] stringArrToDoubleArr(String[] stringArr){
		Integer arrLength = stringArr.length;
		Double[] doubleArr = new Double[arrLength];
		for (int i = 0; i < arrLength; i++){
			doubleArr[i] = Double.parseDouble(stringArr[i]);
		}
		return doubleArr;
	}

	/*
	** Takes a filename and number of rows in triangle.
	** Returns a matrix, where each row in the triangle occupies a row in the matrix
	*/
	public static Double[][] readFile(String fileName, Integer numRows){
		// Using Double because instructions do not specify Integer
		Double[][] triangleData = new Double[numRows][];
		Integer currentRow = 0;
		// Reads file below
        File dataFile = new File(fileName);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            Double[] splitLine;
            while (line != null && currentRow < numRows) {
            	splitLine = stringArrToDoubleArr(line.split("\\s+"));
            	triangleData[currentRow] = splitLine;
            	currentRow++;
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
        return triangleData;
	}

	/*
	** Takes the matrix representing the triangle and returns an object (PathAndValue) containing
	** the path and sum of values leading up to a certain position in the triangle.
	** Uses UCS algorithm from https://algorithmicthoughts.wordpress.com/2012/12/15/artificial-intelligence-uniform-cost-searchucs/
	*/
	public static PathAndValue uniformCostSearch(Double[][] triangle){
		PriorityQueue<PathAndValue> pathQueue = new PriorityQueue<PathAndValue>();
		List<Double> initialPath = new ArrayList<Double>();
		// Assumes only one object in the first row; adds first row
		Double rootVal = triangle[0][0];
		initialPath.add(rootVal);
		PathAndValue maxPath = new PathAndValue(initialPath, rootVal, 0, 0);
		pathQueue.add(maxPath);

		// Traverses down tree
		while(!pathQueue.isEmpty()){
			// maxPath is the best path option for now
			maxPath = pathQueue.poll();
			//System.out.println("Polled object" + maxPath.getPath());

			// If maxPath contains goal, return and exit
			if (maxPath.getRow() + 1 == triangle.length) {
				return maxPath;
			} else {
				Double nextLeftInt = triangle[maxPath.getRow() + 1][maxPath.getIndex()];
				Double nextRightInt = triangle[maxPath.getRow() + 1][maxPath.getIndex() + 1];
				
				// Expands maxPath, adds to priority queue and continues

				// Updates and adds path on left side
				List<Double> leftPathTilNow = new ArrayList<Double>(maxPath.getPath());
				leftPathTilNow.add(nextLeftInt);
				Double leftValue = maxPath.getValue() + nextLeftInt;
				//System.out.println("Adding left" + leftPathTilNow);
				PathAndValue left = new PathAndValue(leftPathTilNow, leftValue, maxPath.getRow() + 1, maxPath.getIndex());
				pathQueue.add(left);

				// Adding path on right side
				List<Double> rightPathTilNow = new ArrayList<Double>(maxPath.getPath());
				rightPathTilNow.add(nextRightInt);
				Double rightValue = maxPath.getValue() + nextRightInt;
				PathAndValue right = new PathAndValue(rightPathTilNow, rightValue, maxPath.getRow() + 1, maxPath.getIndex() + 1);
				pathQueue.add(right);
			}

		}
		return maxPath;
	}

	public static void main(String[] args){
		Double[][] triangleData = readFile("triangle.txt", 100);
		PathAndValue max = uniformCostSearch(triangleData);
		System.out.println("The maximum total from top to bottom of is...");
		System.out.println("Value: " + max.getValue());
		System.out.println("Path: " + max.getPath());
	}
}