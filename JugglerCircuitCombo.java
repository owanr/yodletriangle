import java.io.*;
import java.util.*;
/*
** Written by Risako Owan
** Used in JugglerFest.java
** Polls off smaller object first
*/
public class JugglerCircuitCombo implements Comparable<JugglerCircuitCombo>{
	private String juggler;
	private String circuit;
	private Integer value;

	/*
	** Constructor for JugglerCircuitCombo
	*/
	public JugglerCircuitCombo(String juggler, String circuit, Integer value){
		this.juggler = juggler;
		this.circuit = circuit;
		this.value = value;
	}

	// Getters

	public String getJuggler(){
		return this.juggler;
	}

	public String getCircuit(){
		return this.circuit;
	}

	public Integer getValue(){
		return this.value;
	}

	/*
	** compareTo() allows this JugglerCircuitCombo to be compared to another JugglerCircuitCombo
	** based their HEP values
	*/
	public int compareTo(JugglerCircuitCombo other){
		if(other.getValue()>this.value){
			return -1;
		} else if(other.getValue() == this.value){
			return 0;
		} else {
			return 1;
		}
	}
}