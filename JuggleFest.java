import java.io.*;
import java.util.*;
import java.lang.*;

/*
** @author Risako Owan
** Written on March 24, 2015
** Problem from http://www.yodlecareers.com/puzzles/jugglefest.html
*/
public class JuggleFest{
	/*
	** Takes a filename and returns a list containing the circuit HEP info dictionary, 
	** juggler HEP info dictionary, and juggler preference dictionary.
	*/
	public static List<HashMap> readFile(String fileName) {
		// Initializes variables
		List<HashMap> resultList = new ArrayList<HashMap>();
		HashMap<String, HashMap> circuitDict = new HashMap<String, HashMap>();
		HashMap<String, HashMap> jugglerDict = new HashMap<String, HashMap>();	
		HashMap<String, List<String>> jugglerPref = new HashMap<String, List<String>>();

		// Starts reading file
        File dataFile = new File(fileName);
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            String line = reader.readLine();
            String[] splitLine;
            String[] splitSplitLine;

            // First reads circuit data
            while (line != null && line.length() != 0) {
                splitLine = line.split("\\s+");
                HashMap<String, Integer> circuitInfo = new HashMap<String, Integer>();
                for (int i = 2; i < splitLine.length; i++){
                	splitSplitLine = splitLine[i].split(":");
                	circuitInfo.put(splitSplitLine[0], Integer.parseInt(splitSplitLine[1]));
                }
                circuitDict.put(splitLine[1], circuitInfo);
                line = reader.readLine();
            }

            // Gets rid of space between circuit info and juggler info
            while (line == null || line.length() == 0){
            	line = reader.readLine();
            }

            // Collects and stores info on jugglers
            while (line != null && line.length() != 0) {
				splitLine = line.split("\\s+");
                HashMap<String, Integer> jugglerInfo = new HashMap<String, Integer>();

                // Collects skill info
                for (int i = 2; i < splitLine.length - 1; i++){
                	splitSplitLine = splitLine[i].split(":");
                	jugglerInfo.put(splitSplitLine[0], Integer.parseInt(splitSplitLine[1]));
                }
                jugglerDict.put(splitLine[1], jugglerInfo);

                // Collects preference info
                splitSplitLine = splitLine[splitLine.length - 1].split(",");
                //System.out.println(splitLine[1] + " with " + Arrays.toString(splitSplitLine));
                List<String> list = new LinkedList<String>(Arrays.asList(splitSplitLine));
                jugglerPref.put(splitLine[1], list);
            	line = reader.readLine();
            }
            // Store in resultList once all info is collected
            resultList.add(circuitDict);
            resultList.add(jugglerDict);
            resultList.add(jugglerPref);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
        return resultList;
    }

    /*
    ** Takes the dictionary of jugglers and creates a queue of the juggler names (no skill or preferences)
    ** Used in method: assign
    */
    public static Queue<String> createQueueOfJugglers(HashMap<String, HashMap> jugglerDict){
    	Queue<String> jugglerQueue = new LinkedList<String>();
    	for (String jugglerID : jugglerDict.keySet()){
    		jugglerQueue.add(jugglerID);
    	}
    	return jugglerQueue;
    }

    /*
    ** Creates an array containing an empty queue for each circuit
    ** Used in method: assign
    ** This array will later store all the assigned jugglers for each circuit
    */
    @SuppressWarnings("unchecked")
    public static PriorityQueue<JugglerCircuitCombo>[] createPQofCircuits(HashMap<String, HashMap> circuitDict){
    	Integer numCircuits = circuitDict.size();
    	PriorityQueue<JugglerCircuitCombo>[] circuitPQArray = new PriorityQueue[numCircuits];
    	for (int i = 0; i < numCircuits; i++){
    		PriorityQueue<JugglerCircuitCombo> newPQ = new PriorityQueue<JugglerCircuitCombo>();
    		circuitPQArray[i] = newPQ;
    	}
    	return circuitPQArray;
    }

	/*
    ** Takes the HEP info for a circuit and for a juggler and calculates the dot product
    ** Decided to call this everytime, because dataset was fairly small 
    */
    public static Integer calculateDotProduct(HashMap<String, Integer> circuitDict, HashMap<String, Integer> jugglerDict){
    	Integer dotProduct = 0;
    	for (String skill : circuitDict.keySet()){
    		dotProduct += circuitDict.get(skill)*jugglerDict.get(skill);
    	}
    	return dotProduct;
    }

    @SuppressWarnings("unchecked")
    public static PriorityQueue<JugglerCircuitCombo> findCircuitGroupsWithSpace(HashMap<String, HashMap> circuitDict, HashMap<String, HashMap> jugglerDict,
        PriorityQueue<JugglerCircuitCombo>[] circuitPQArray, List<String> sadJugglerList, Integer numPerCircuit){
        PriorityQueue<JugglerCircuitCombo> circuitGroupsWithSpace = new PriorityQueue<JugglerCircuitCombo>();
        // circuitPQArray.size() is the number of circuits
        for (int i = 0; i < circuitPQArray.length; i++){
            if (circuitPQArray[i].size() < numPerCircuit){
                for (String eachJuggler : sadJugglerList){
                    String thisCircuit = "C"+i;
                    Integer curDotProduct = calculateDotProduct(circuitDict.get(thisCircuit), jugglerDict.get(eachJuggler));
                    JugglerCircuitCombo curCombo = new JugglerCircuitCombo(eachJuggler, thisCircuit, curDotProduct);
                    circuitGroupsWithSpace.add(curCombo);
                }
            }
        }
        return circuitGroupsWithSpace;
    }

    /*
    ** Takes in a dictionary of circuit HEP info, juggler HEP info, and juggler preference
    ** Returns an array specifying the assigned jugglers for each circuit
    ** Find each circuits group using its index number
    */
    @SuppressWarnings("unchecked")
    public static PriorityQueue<JugglerCircuitCombo>[] assign(HashMap<String, HashMap> circuitDict, HashMap<String, HashMap> jugglerDict, HashMap<String, List<String>> jugglerPref){
    	// Can't shallow copy jugglerPref for some reason...
    	// HashMap<String, List<String>> jugglerPref = new HashMap<String, List<String>>(jPref);
    	// HashMap<String, List<String>> jugglerPref = (HashMap<String, List<String>>)jPref.clone();
    	// System.out.println("initial jPref"+jPref.get("J10685").size());
    	// System.out.println("initial jugglerPref"+jugglerPref.get("J10685").size());

    	Integer numPerCircuit = jugglerDict.size()/circuitDict.size();
        Queue<String> jugglerQueue = createQueueOfJugglers(jugglerDict);
    	List<String> sadJugglerList = new ArrayList<String>();
    	PriorityQueue<JugglerCircuitCombo>[] circuitPQArray = createPQofCircuits(circuitDict);

    	while (!jugglerQueue.isEmpty()){
    		String currentJuggler = jugglerQueue.poll();	
    		String currentJHighestPref = jugglerPref.get(currentJuggler).get(0);
    		Integer highestPrefIndex = Integer.parseInt(currentJHighestPref.replace("C",""));
			Integer curDotProduct = calculateDotProduct(circuitDict.get(currentJHighestPref), jugglerDict.get(currentJuggler));
			//System.out.println(currentJuggler + " with " + currentJHighestPref + " and " + curDotProduct);
			JugglerCircuitCombo curCombo = new JugglerCircuitCombo(currentJuggler, currentJHighestPref, curDotProduct);
			PriorityQueue<JugglerCircuitCombo> currentPQ = circuitPQArray[highestPrefIndex];
		
			currentPQ.add(curCombo);
			// If there are more than numPerCircuit jugglers assigned...
			if (currentPQ.size() == numPerCircuit + 1){
				JugglerCircuitCombo replacedJuggler = currentPQ.poll();
				String replacedJugglerName = replacedJuggler.getJuggler();
				List<String> pref = jugglerPref.get(replacedJugglerName);
				pref.remove(0);
                if (!pref.isEmpty()){
    				jugglerPref.put(replacedJugglerName, pref);
    				jugglerQueue.add(replacedJugglerName);
                } else {
                    sadJugglerList.add(replacedJugglerName);
                }
			}
			circuitPQArray[highestPrefIndex] = currentPQ;
    	}

        // Now assigning jugglers who couldn't get their preferred circuits
        // Assigns those with higher dot products first
        PriorityQueue<JugglerCircuitCombo> circuitGroupsWithSpace = findCircuitGroupsWithSpace(circuitDict, jugglerDict, circuitPQArray, sadJugglerList, numPerCircuit);
        while (!circuitGroupsWithSpace.isEmpty()) {
            JugglerCircuitCombo thisCombo = circuitGroupsWithSpace.poll();
            String thisJuggler = thisCombo.getJuggler();
            Integer circuitIndex = Integer.parseInt(thisCombo.getCircuit().replace("C",""));
            if (circuitPQArray[circuitIndex].size() < numPerCircuit && sadJugglerList.contains(thisJuggler)){
                circuitPQArray[circuitIndex].add(thisCombo);
                sadJugglerList.remove(thisJuggler);
            }
        }

    	// Testing print statements for shallow copy test
    	// System.out.println("afterwards jPref"+jPref.get("J10685").size());
    	// System.out.println("afterwards jugglerPref"+jugglerPref.get("J10685").size());

    	return circuitPQArray;
    }


    /*
    ** Prints the juggler group for circuit 1970 and also each juggler's dot product of his/her skills and circuit HEP requirements
    */
    @SuppressWarnings("unchecked")
    public static void printResultsFor1970(HashMap<String, HashMap> circuitDict, HashMap<String, HashMap> jugglerDict,
    	HashMap<String, List<String>> jugglerPref, PriorityQueue<JugglerCircuitCombo>[] assignedGroups){
		String circuit = "C"+1970;
		System.out.print(circuit + " ");
		PriorityQueue<JugglerCircuitCombo> currentPQ = assignedGroups[1970];
		while (!currentPQ.isEmpty()){
			String curJuggler = currentPQ.poll().getJuggler();
			// Adding newline for easier viewing
			System.out.print("\n"+curJuggler);
			List<String> curJugPref = jugglerPref.get(curJuggler);
			for (String pref : curJugPref) {
				Integer curDotProduct = calculateDotProduct(circuitDict.get(pref), jugglerDict.get(curJuggler));
				System.out.print(" " + pref + ":" + curDotProduct);
			}
			if (!currentPQ.isEmpty()){
				System.out.print(", ");
			}
		}
		System.out.println();
    }

    /*
    ** Almost identical to printResultsFor1970
    ** Used for when testing on small example (given on website)
    ** Should not be used with larger dataset (will take a long time)
    */
    @SuppressWarnings("unchecked")
    public static void printResultsForAll(HashMap<String, HashMap> circuitDict, HashMap<String, HashMap> jugglerDict,
    	HashMap<String, List<String>> jugglerPref, PriorityQueue<JugglerCircuitCombo>[] assignedGroups){
    	for (int i = 0; i < assignedGroups.length; i++){
    		String circuit = "C"+i;
    		System.out.print(circuit + " ");
    		PriorityQueue<JugglerCircuitCombo> currentPQ = assignedGroups[i];
			while (!currentPQ.isEmpty()){
				String curJuggler = currentPQ.poll().getJuggler();
				System.out.print(curJuggler);
				List<String> curJugPref = jugglerPref.get(curJuggler);
				for (String pref : curJugPref) {
					Integer curDotProduct = calculateDotProduct(circuitDict.get(pref), jugglerDict.get(curJuggler));
					System.out.print(" " + pref + ":" + curDotProduct);
				}
				if (!currentPQ.isEmpty()){
					System.out.print(", ");
				}
			}
    		System.out.println();
    	}
    }

    @SuppressWarnings("unchecked")
	public static void main(String[] args){


		// REPLACE "realjuggleFest.txt" WITH ACTUAL FILE NAME-------------------------------------------------------------------------------
		List<HashMap> filledDicts = readFile("realjuggleFest.txt");
		// Calling readFile twice because jugglerPref kept on getting modified within the assign function(?) and could not be shallow copied.
		List<HashMap> filledDicts2 = readFile("realjuggleFest.txt");


		HashMap<String, HashMap> circuitDict = filledDicts.get(0);
		HashMap<String, HashMap> jugglerDict = filledDicts.get(1);
		HashMap<String, List<String>> jugglerPref = filledDicts.get(2);
		HashMap<String, List<String>> jugglerPref2 = filledDicts2.get(2);;

		//System.out.println("before: "+jugglerPref.get("J10685"));
		PriorityQueue<JugglerCircuitCombo>[] assignedGroups = assign(circuitDict, jugglerDict, jugglerPref);
		// Using jugglerPref2 because jugglerPref is modified here
		printResultsFor1970(circuitDict, jugglerDict, jugglerPref2, assignedGroups);
		//System.out.println("after "+jugglerPref2.get("J10685"));
	}
}