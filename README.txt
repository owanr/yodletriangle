1. Triangle

Please use the following commands to run the program:

javac YodleTriangle.java
java YodleTriangle

The zip file for this problem contains:
YodleTriangle.java -- the main program; prints out the maximum total for the path from the top of the triangle to the bottom.

PathAndValue.java -- a program that creates an object used in YodleTriangle.java

triangle.txt -- the given textfile containing the 100-row triangle; included to avoid any errors that would be caused by a different filename.





2. Juggle Fest

Please use the following commands to run the program:

javac JuggleFest.java
java JuggleFest

The zip file for this problem contains:
JuggleFest.java -- the main program; prints out the group for C1970 and creates the textfile for all the circuit-juggler matchings.

JugglerCircuitCombo.java -- a program that creates an object used in JuggleFest.java

owanJuggleFestOutput.txt -- the output textfile created after running JuggleFest.java

realjugglefest.txt -- the given textfile containing all information on the circuits and jugglers; included to avoid any errors that would be caused by a different filename.