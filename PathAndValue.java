import java.io.*;
import java.util.*;
/*
** Written by Risako Owan
** Used in YodleTriangle.java
** Polls off larger object first
*/
public class PathAndValue implements Comparable<PathAndValue>{
	private List<Double> path;
	private Double value;
	private Integer currentRow;
	private Integer currentIndex;

	/*
	** Constructor for PathAndValue
	*/
	public PathAndValue(List<Double> path, Double value, Integer currentRow, Integer currentIndex){
		this.path = path;
		this.value = value;
		this.currentRow = currentRow;
		this.currentIndex = currentIndex;
	}

	// Getters
	public List<Double> getPath(){
		return this.path;
	}

	public Double getValue(){
		return this.value;
	}

	public Integer getRow(){
		return this.currentRow;
	}

	public Integer getIndex(){
		return this.currentIndex;
	}
	/*
	** compareTo() allows this PathAndValue to be compared to another PathAndValue based on their values
	*/
	public int compareTo(PathAndValue other){
		if(other.getValue()<this.value){
			return -1;
		} else if(other.getValue() == this.value){
			return 0;
		} else {
			return 1;
		}
	}
}